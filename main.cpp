#include <iostream>
#include <fstream>
#include <random>
#include <cstdlib>
#include <chrono>

const size_t SIZE_OF_IMAGE = 20;
const size_t SIZE_OF_HEADER = 14;
const size_t SIZE_OF_INFO_HEADER = 40;
char* BLACK = (char*)"\x00\x00\x00";
char* WHITE = (char*)"\xFF\xFF\xFF";

void createBmp(const std::string* filename);
char* readBmp(const std::string* filename);

void printImage(const char *image);

int32_t getRandomNum(int32_t min, int32_t max)
{
    const static auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::mt19937_64 generator(seed);
    std::uniform_int_distribution<int32_t> dis(min, max);
    return dis(generator);
};

int main() {
    auto FILENAME = new std::string("test.bmp");
    createBmp(FILENAME);
    auto image = readBmp(FILENAME);
    printImage(image);
    delete FILENAME;
    delete[] image;
    return 0;
}

void printImage(const char *image) {
    for (size_t y = 0; y < SIZE_OF_IMAGE * 3; y += 3) {
        for (size_t x = 0; x < SIZE_OF_IMAGE * 3; x += 3) {
            std::cout << (*(image + y * SIZE_OF_IMAGE + x) == char(255) ? (char)254 : ' ');
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

char* readBmp(const std::string* filename) {
    char* header = new char;
    char* info_header = new char;
    char* image = new char;
    std::ifstream fin(*filename, std::ios_base::binary);
    if (fin.is_open())
    {
        fin.read(header, SIZE_OF_HEADER);
        fin.read(info_header, SIZE_OF_INFO_HEADER);
        fin.read(image, 3 * SIZE_OF_IMAGE * SIZE_OF_IMAGE);
        fin.close();
    }
    return image;
}

void createBmp(const std::string* filename) {
    const size_t BITS_IN_BYTE = 8;
    int filesize = 54 + 3 * SIZE_OF_IMAGE * SIZE_OF_IMAGE;
    char BMP_HEADER[SIZE_OF_HEADER] = {/*format*/'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, /*data start*/54, 0, 0, 0};
    char BMP_INFO[SIZE_OF_INFO_HEADER] = {SIZE_OF_INFO_HEADER, 0, 0, 0, /*width*/0, 0, 0, 0, /*height*/0, 0, 0, 0, /*the number of color planes (must be 1) */1, 0, /*the number of bits per pixel, which is the color depth of the imag*/24, 0};
    BMP_HEADER[2] = char(filesize);
    BMP_HEADER[3] = char(filesize >> BITS_IN_BYTE);
    BMP_HEADER[4] = char(filesize >> (BITS_IN_BYTE * 2));
    BMP_HEADER[5] = char(filesize >> (BITS_IN_BYTE * 3));

    // Width.
    BMP_INFO[4] = char(SIZE_OF_IMAGE);
    BMP_INFO[5] = char(SIZE_OF_IMAGE >> BITS_IN_BYTE);
    BMP_INFO[6] = char(SIZE_OF_IMAGE >> (BITS_IN_BYTE * 2));
    BMP_INFO[7] = char(SIZE_OF_IMAGE >> (BITS_IN_BYTE * 3));
    // Height.
    BMP_INFO[8] = char(SIZE_OF_IMAGE);
    BMP_INFO[9] = char(SIZE_OF_IMAGE >> BITS_IN_BYTE);
    BMP_INFO[10] = char(SIZE_OF_IMAGE >> (BITS_IN_BYTE * 2));
    BMP_INFO[11] = char(SIZE_OF_IMAGE >> (BITS_IN_BYTE * 3));

    size_t x_start = getRandomNum(1, (SIZE_OF_IMAGE / 2) - 2);
    size_t x_end = getRandomNum((SIZE_OF_IMAGE / 2), SIZE_OF_IMAGE - 2);
    size_t y_start = getRandomNum(1, (SIZE_OF_IMAGE / 2) - 2);
    size_t y_end = getRandomNum((SIZE_OF_IMAGE / 2), SIZE_OF_IMAGE - 2);

    std::ofstream fout(*filename, std::ios_base::binary);
    if (fout.is_open()) {
        fout.write(BMP_HEADER, SIZE_OF_HEADER);
        fout.write(BMP_INFO, SIZE_OF_INFO_HEADER);
    }
    for (size_t y = 0; y < SIZE_OF_IMAGE; y++) {
        for (size_t x = 0; x < SIZE_OF_IMAGE; x++) {
            char* color_to_write;
            if (y >= y_start && y <= y_end && x >= x_start && x <= x_end) {
                color_to_write = BLACK;
            }
            else {
                color_to_write = WHITE;
            }
            fout.write(color_to_write, strlen(WHITE));
        }
    }
    fout.close();
}
